using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Desafio_potencial.Entities;
using Desafio_potencial.Context;

namespace Desafio_potencial.Controllers
{   
    [ApiController]  // serve para indentificar que é uma APi controller
    [Route("[controller]")] // requisito para criação de API
    public class VendasOnlineControllers :ControllerBase
    {
        private readonly VendasOnlineContext _context;

        public VendasOnlineControllers( VendasOnlineContext context)
        {
            _context = context;
        }



        [HttpPost("CadastrarVendedor")]// porque estamos enviando uma informação
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor); // adicionar no banco de dados
            _context.SaveChanges(); // salvar
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = vendedor.VendedorId }, vendedor);
        }
        [HttpPost("CadastrarCliente")]// porque estamos enviando uma informação
        public IActionResult CadastrarCliente(Cliente cliente)
        {
            _context.Add(cliente); // adicionar no banco de dados
            _context.SaveChanges(); // salvar
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = cliente.ClienteId }, cliente);
        }        

        [HttpPost("CadastrarProdutos")] // porque estamos enviando uma informação
        public IActionResult CadastrarProduto(Produtos produtos)
        {   
            

            _context.Add(produtos); // adicionar no banco de dados
            _context.SaveChanges(); // salvar
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = produtos.ProdutoId  }, produtos);

        }
        [HttpPost("CadastrarPedido")] // porque estamos enviando uma informação
        public IActionResult CadastrarPedido(PedidoRealizado pedidoRealizado)
        {   
            
            var LocalizarCliente = _context.Clientes.Find(pedidoRealizado.ClienteId); 
            var LocalizarVendedor = _context.Vendedor.Find(pedidoRealizado.VendedorId); 

             if (LocalizarCliente == null)
             {
                return BadRequest(new { Erro = "Não foi Localizar nenhum cliente" });    
             }
             if (LocalizarVendedor == null)
             {
                return BadRequest(new { Erro = "Não foi Localizar nenhum Vendedor" });    
             }

            _context.Add(pedidoRealizado); // adicionar no banco de dados
            _context.SaveChanges(); // salvar
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = pedidoRealizado.PedidoRealizadoId }, pedidoRealizado);

        }

        [HttpPost("AdicionarProdutoCarrinho")]     
        //public IActionResult AdicionarCarrinho(int PedidoId, int produtoId, ItensDoPedido itensDoPedido)
        public IActionResult AdicionarCarrinho( ItensDoPedido itensDoPedido)
        {   
            var pedidoLocalizado = _context.PedidoRealizado.Find(itensDoPedido.PedidoRealizadoId);
            var produtoLocalizado = _context.Produtos.Find(itensDoPedido.ProdutoId);
            var clienteLocalizado = _context.Clientes.Find(itensDoPedido.ClienteId);

                            
             if (pedidoLocalizado == null)
             {
                return BadRequest(new { Erro = "Não foi Localizar nenhum pedido" });    
             }
             if (produtoLocalizado == null)
             {
                return BadRequest(new { Erro = "Não foi Localizar nenhum produto" });    
             }
             if (clienteLocalizado == null)
             {
                return BadRequest(new { Erro = "Não foi Localizar nenhum Cliente" });    
             }
            
            itensDoPedido.PedidoRealizadoId = pedidoLocalizado.PedidoRealizadoId ;
            itensDoPedido.ProdutoId= produtoLocalizado.ProdutoId;
            itensDoPedido.ClienteId = clienteLocalizado.ClienteId;


            _context.Add(itensDoPedido); // adicionar no banco de dados
            _context.SaveChanges(); // salvar
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = itensDoPedido.PedidoRealizadoId }, itensDoPedido);
        }
        [HttpGet("ObterVendedorPor{id}")]
        public IActionResult ObterVendedorPorId(int id)
        {
             var vendedor = _context.Vendedor.Find(id); //.Contatos é o nosso dbset
            if(vendedor == null) // se ele não achar ele vai retornar um NotFound
            {
                return NotFound();
            }
                    return Ok(vendedor);
        }
        [HttpGet("VendaPor{id}")]
        public IActionResult ObterPedidodaPorId(int id)
        {   
            
             var pedidoLocalizado = _context.PedidoRealizado.Find(id); 

            if(pedidoLocalizado == null) // se ele não achar ele vai retornar um NotFound
            {
                return BadRequest(new { Erro = "Não foi Localizar nenhum pedido" });  
            }
             //var itensDoPedido = _context.ItensDoPedidos.Find(pedidoLocalizado.PedidoRealizadoId);,
             var itensDoPedido = _context.ItensDoPedidos.Where(x => x.PedidoRealizadoId == pedidoLocalizado.PedidoRealizadoId);
             var localizarVendedor = _context.Vendedor.Find(pedidoLocalizado.VendedorId);
             var clienteLocalizado = _context.Clientes.Find(pedidoLocalizado.ClienteId);

             
             
             //var LocalizarProduto = _context.Produtos.Where(x => x.ProdutoId == itensDoPedido.ProdutoId);

            foreach (var item in itensDoPedido)
            
            {
                             
            }

                    return Ok(
                        new
                        {   clienteLocalizado,
                            pedidoLocalizado,
                            itensDoPedido,
                            localizarVendedor

                        }
                    );
                    
        }   


    }
}