using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Desafio_potencial.Entities
{
    public class Produtos
    {   [Key]
        public int ProdutoId { get; protected set;} 
        public string Produto { get; set; }
        public string Descricao { get; set; }
    }
}