using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Desafio_potencial.Entities
{
    public class PedidoRealizado
    {   
        [Key]
        public int PedidoRealizadoId { get; protected set; } 
        
        [ForeignKey("ClienteId")]
        public int ClienteId { get;  set; } 
        [ForeignKey("VendedorId")]
        public int VendedorId { get; set;}
        public DateTime Data { get; set; }
        public string StatusVenda { get; set; }
                
        
    }
}