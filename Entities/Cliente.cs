using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.ComponentModel.DataAnnotations;

namespace Desafio_potencial.Entities
{
    public class Cliente
    
    {   
        [Key]
        public int ClienteId  { get; protected set; } //Utilizado protected set para o Entity reconhecer como uma ID
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string E_mail { get; set; }
        public string Telefone { get; set; }
        
        
        
       
    }
}