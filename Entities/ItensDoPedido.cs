using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Desafio_potencial.Entities
{
    public class ItensDoPedido
    {
       [Key]
       public int ItensDoPedidoId { get; protected set; } 
       [ForeignKey("ProdutoId")]
        public int ProdutoId { get; set ;} //Utilizado protected set para o Entity reconhecer como uma ID
        [ForeignKey("PedidoRealizadoId")]
        public int PedidoRealizadoId { get; set;}
        [ForeignKey("ClienteId")]
        public int ClienteId { get; set;}


    }
}