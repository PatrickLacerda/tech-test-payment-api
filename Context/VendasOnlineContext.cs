using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio_potencial.Entities;
using Microsoft.EntityFrameworkCore;

namespace Desafio_potencial.Context
{
    public class VendasOnlineContext :DbContext
    
    {
         public VendasOnlineContext(DbContextOptions<VendasOnlineContext>options) : base(options) // aqui onde recebe a conexão com banco
        //faz a ligação do banco de dados, vai passar para calsse pai do DbContext
        {
            
        }
        public DbSet<Cliente> Clientes{get;set;} 
        public DbSet<ItensDoPedido> ItensDoPedidos{get;set;} 
        public DbSet<PedidoRealizado> PedidoRealizado{get;set;} 
        public DbSet<Produtos> Produtos{get;set;}
        public DbSet<Vendedor> Vendedor{get;set;} 
        
        
    }
}

